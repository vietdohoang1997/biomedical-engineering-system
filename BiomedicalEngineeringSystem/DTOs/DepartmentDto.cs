﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.DTOs
{
    public class DepartmentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}