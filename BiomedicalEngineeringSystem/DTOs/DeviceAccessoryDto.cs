﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.DTOs
{
    public class DeviceAccessoryDto
    {
        public int DeviceId { get; set; }

        public int AccessoryId { get; set; }

        public string Note { get; set; }

        public DeviceDto Device { get; set; }

        public AccessoryDto Accessory { get; set; }
    }
}