﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.DTOs
{
    public class DeviceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public string Serial { get; set; }
        public string Country { get; set; }
        public string Manufacture { get; set; }
        public DateTime? ProduceDate { get; set; }
        public DateTime? ImportedDate { get; set; }
        public DateTime? HandoverDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int? Status { get; set; }
        public decimal Price { get; set; }
        public string Note { get; set; }

        public int ProviderId { get; set; }
        public ProviderDto Provider { get; set; }

        public int? TypeId { get; set; }
        public virtual DeviceTypeDto DeviceType { get; set; }

        public virtual ICollection<DeviceAccessoryDto> DeviceAccessories { get; set; }

        public int DepartmentId { get; set; }
        public virtual DepartmentDto Department { get; set; }

    }

    public class NotificationDto
    {
        public int Id { get; set; }
        public string DeviceName { get; set; }
        public string Department { get; set; }
        public DateTime Time { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
    }
}