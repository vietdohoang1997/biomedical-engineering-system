﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class DepartmentsController : ApiController
    {
        private SystemDBContext _context;

        public DepartmentsController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/departments")]
        public DataResponse<List<Department>> GetAllDepartments(string query = null)
        {
            var departmentQuery = _context.Departments.AsQueryable();

            if (!String.IsNullOrWhiteSpace(query))
                departmentQuery = departmentQuery.Where(c => c.Name.Contains(query));


            return DataResponse<List<Department>>.GetResult(1, "OK", departmentQuery.ToList().Count(), departmentQuery.ToList());
        }

        [HttpGet]
        [Route("api/departments/{id:int}")]
        public DataResponse<Department> GetDepartmentById(int id)
        {
            var departmentQuery = _context.Departments.SingleOrDefault(d => d.Id == id);

            if (departmentQuery == null)
                return DataResponse<Department>.GetResult(-1, "Not Found", null);

            return DataResponse<Department>.GetResult(1, "OK", 1, departmentQuery);
        }

        [HttpGet]
        [Route("api/departments/paging")]
        public DataResponse<List<Department>> GetDepartmentByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var total = _context.Departments.ToList();
            
            var departmentQuery = _context.Departments.Where(d => d.Name.Contains(textSearch)).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();
            
            if (departmentQuery.Count == 0)
                return DataResponse<List<Department>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<Department>>.GetResultV2(1, "OK",total.Count, departmentQuery.Count, departmentQuery);
        }

        [HttpPost]
        [Route("api/departments")]
        // GET api/<controller>
        public DataResponse<Department> CreateNewDepartment(Department newDepartment)
        {

            if (!ModelState.IsValid)
                return DataResponse<Department>.GetResult(-1, "Model is not valid", null, null);

            _context.Departments.Add(newDepartment);
            _context.SaveChanges();

            return DataResponse<Department>.GetResult(1, "Created", 1, newDepartment);
        }


        [HttpPut]
        [Route("api/departments/{id:int}")]
        public DataResponse<Department> UpdateDepartment(int id, Department updateDepartment)
        {
            if (!ModelState.IsValid)
                return DataResponse<Department>.GetResult(-1, "Model is not valid", null, null);

            var departmentInDb = _context.Departments.SingleOrDefault(d => d.Id == id);

            if (departmentInDb == null)
                return DataResponse<Department>.GetResult(-1, "Not Found", null, null);

            departmentInDb.Name = updateDepartment.Name;
            departmentInDb.Address = updateDepartment.Address;

            _context.SaveChanges();

            return DataResponse<Department>.GetResult(1, "Updated", 1, updateDepartment);
        }

        [HttpDelete]
        [Route("api/departments/{id:int}")]
        public DataResponse<Department> DeleteDepartment(int id)
        {
            try
            {
                var departmentQuery = _context.Departments.SingleOrDefault(d => d.Id == id);

                if (departmentQuery == null)
                    return DataResponse<Department>.GetResult(-1, "Not Found", null, null);

                _context.Departments.Remove(departmentQuery);

                _context.SaveChanges();

                return DataResponse<Department>.GetResult(1, "Deleted", null);
            }
            catch (Exception ex)
            {
                return DataResponse<Department>.GetResult(1, ex.Message, null);
            }
        }
    }
}
