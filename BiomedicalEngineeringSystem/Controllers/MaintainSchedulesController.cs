﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class MaintainSchedulesController : ApiController
    {
        private SystemDBContext _context;

        public MaintainSchedulesController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/maintain_schedules")]
        public DataResponse<List<MaintainSchedule>> GetAllSchedules()
        {
            var deviceQuery = _context.MaintainSchedules.AsQueryable();

            return DataResponse<List<MaintainSchedule>>.GetResult(1, "OK", deviceQuery.ToList().Count(), deviceQuery.ToList());
        }

        [HttpGet]
        [Route("api/maintain_schedules/{id:int}")]
        public DataResponse<MaintainSchedule> GetScheduleById(int id)
        {
            var deviceQuery = _context.MaintainSchedules.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<MaintainSchedule>.GetResult(-1, "Not Found", null);

            return DataResponse<MaintainSchedule>.GetResult(1, "OK", 1, deviceQuery);
        }

        [HttpGet]
        [Route("api/maintain_schedules/paging")]
        public DataResponse<List<MaintainSchedule>> GetDepartmentByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var total = _context.MaintainSchedules.ToList();

            var departmentQuery = _context.MaintainSchedules.Where(d => d.DeviceName.Contains(textSearch)).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            if (departmentQuery.Count == 0)
                return DataResponse<List<MaintainSchedule>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<MaintainSchedule>>.GetResultV2(1, "OK", total.Count, departmentQuery.Count, departmentQuery);
        }

        [HttpPost]
        [Route("api/maintain_schedules")]
        // GET api/<controller>
        public DataResponse<MaintainSchedule> CreateNewSchedule(MaintainSchedule newSchedule)
        {

            if (!ModelState.IsValid)
                return DataResponse<MaintainSchedule>.GetResult(-1, "Model is not valid", null, null);

            var deviceInDb = _context.Devices.SingleOrDefault(d => d.Name == newSchedule.DeviceName);
            if (deviceInDb != null)
            {
                deviceInDb.Status = 3;
            }

            newSchedule.Status = 1;
            _context.MaintainSchedules.Add(newSchedule);
            _context.SaveChanges();

            return DataResponse<MaintainSchedule>.GetResult(1, "Created", 1, newSchedule);
        }


        [HttpPut]
        [Route("api/maintain_schedules/{id:int}")]
        public DataResponse<MaintainSchedule> UpdateSchedule(int id, MaintainSchedule updateSchedule)
        {
            if (!ModelState.IsValid)
                return DataResponse<MaintainSchedule>.GetResult(-1, "Model is not valid", null, null);

            var maintainInDb = _context.MaintainSchedules.SingleOrDefault(d => d.Id == id);

            if (maintainInDb == null)
                return DataResponse<MaintainSchedule>.GetResult(-1, "Not Found", null, null);

            maintainInDb.Status = updateSchedule.Status;

            var deviceInDb = _context.Devices.SingleOrDefault(d => d.Name == updateSchedule.DeviceName);

            if (deviceInDb != null)
            {
                if (updateSchedule.Status == 2)
                {
                    deviceInDb.Status = 4;
                }

                if (updateSchedule.Status == 3)
                {
                    deviceInDb.Status = 1;
                }
            }


            _context.SaveChanges();

            return DataResponse<MaintainSchedule>.GetResult(1, "Updated", 1, updateSchedule);
        }

        [HttpDelete]
        [Route("api/maintain_schedules/{id:int}")]
        public DataResponse<MaintainSchedule> DeleteSchedule(int id)
        {
            var deviceQuery = _context.MaintainSchedules.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<MaintainSchedule>.GetResult(-1, "Not Found", null, null);

            _context.MaintainSchedules.Remove(deviceQuery);

            _context.SaveChanges();

            return DataResponse<MaintainSchedule>.GetResult(1, "Deleted", null);
        }
    }
}
