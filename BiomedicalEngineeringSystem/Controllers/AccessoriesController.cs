﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class AccessoriesController : ApiController
    {
        private SystemDBContext _context;

        public AccessoriesController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/accessories")]
        public DataResponse<List<Accessory>> GetAllAccessories(string query = null)
        {
            var deviceQuery = _context.Accessories.AsQueryable();

            if (!String.IsNullOrWhiteSpace(query))
                deviceQuery = deviceQuery.Where(c => c.Name.Contains(query));


            return DataResponse<List<Accessory>>.GetResult(1, "OK", deviceQuery.ToList().Count(), deviceQuery.ToList());
        }

        [HttpGet]
        [Route("api/accessories/{id:int}")]
        public DataResponse<Accessory> GetAccessoryById(int id)
        {
            var deviceQuery = _context.Accessories.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Accessory>.GetResult(-1, "Not Found", null);

            return DataResponse<Accessory>.GetResult(1, "OK", 1, deviceQuery);
        }

        [HttpGet]
        [Route("api/accessories/paging")]
        public DataResponse<List<Accessory>> GetAccessoriesByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var total = _context.Accessories.ToList();

            var accessoriesQuery = _context.Accessories.Where(d => d.Name.Contains(textSearch)).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            if (accessoriesQuery.Count == 0)
                return DataResponse<List<Accessory>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<Accessory>>.GetResultV2(1, "OK", total.Count, accessoriesQuery.Count, accessoriesQuery);
        }

        [HttpPost]
        [Route("api/accessories")]
        // GET api/<controller>
        public DataResponse<Accessory> CreateNewAccessory(Accessory newAccessory)
        {

            if (!ModelState.IsValid)
                return DataResponse<Accessory>.GetResult(-1, "Model is not valid", null);

            _context.Accessories.Add(newAccessory);
            _context.SaveChanges();

            return DataResponse<Accessory>.GetResult(1, "Created", 1, newAccessory);
        }


        [HttpPut]
        [Route("api/accessories/{id:int}")]
        public DataResponse<Accessory> UpdateAccessory(int id, Accessory updateAccessory)
        {
            if (!ModelState.IsValid)
                return DataResponse<Accessory>.GetResult(-1, "Model is not valid", null, null);

            var deviceInDb = _context.Accessories.SingleOrDefault(d => d.Id == id);

            if (deviceInDb == null)
                return DataResponse<Accessory>.GetResult(-1, "Not Found", null, null);

            deviceInDb.Name = updateAccessory.Name;
            deviceInDb.Size = updateAccessory.Size;
            deviceInDb.Number = updateAccessory.Number;
            deviceInDb.ImportedDate = updateAccessory.ImportedDate;
            deviceInDb.Status = updateAccessory.Status;
            deviceInDb.ProviderId = updateAccessory.ProviderId;

            _context.SaveChanges();

            return DataResponse<Accessory>.GetResult(1, "Updated", 1, updateAccessory);
        }

        [HttpDelete]
        [Route("api/accessories/{id:int}")]
        public DataResponse<Accessory> DeleteAccessory(int id)
        {
            var deviceQuery = _context.Accessories.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Accessory>.GetResult(-1, "Not Found", null, null);

            _context.Accessories.Remove(deviceQuery);

            _context.SaveChanges();

            return DataResponse<Accessory>.GetResult(1, "Deleted", null);
        }
    }
}
