﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using BiomedicalEngineeringSystem.DTOs;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class DevicesController : ApiController
    {
        private SystemDBContext _context;

        public DevicesController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/devices")]
        public DataResponse<List<Device>> GetAllDevices(string query = null)
        {
            var deviceQuery = _context.Devices.AsQueryable();

            if (!String.IsNullOrWhiteSpace(query))
                deviceQuery = deviceQuery.Where(c => c.Name.Contains(query));


            return DataResponse<List<Device>>.GetResult(1, "OK", deviceQuery.ToList().Count(), deviceQuery.ToList());
        }

        [HttpGet]
        [Route("api/devices/{id:int}")]
        public DataResponse<Device> GetDeviceById(int id)
        {
            var deviceQuery = _context.Devices.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Device>.GetResult(-1, "Not Found", null, null);

            return DataResponse<Device>.GetResult(1, "OK", 1, deviceQuery);
        }

        [HttpPost]
        [Route("api/devices")]
        // GET api/<controller>
        public DataResponse<Device> CreateNewDevice(Device newDevice)
        {

            if (!ModelState.IsValid)
                return DataResponse<Device>.GetResult(-1, "Model is not valid", null, null);

            newDevice.Status = 1;
            _context.Devices.Add(newDevice);
            _context.SaveChanges();

            return DataResponse<Device>.GetResult(1, "Created", null, newDevice);
        }

        [HttpGet]
        [Route("api/devices/paging/{departmentId}")]
        public DataResponse<List<Device>> GetDeviceByPaging(int? departmentId, int pageIndex, int pageSize, string textSearch = "")
        {

            var department = _context.Departments.FirstOrDefault(c => c.Name == "Phòng Thiết Bị");

            if(department==null)
                return DataResponse<List<Device>>.GetResult(-1, "Không có phòng thiết bị", null);

            var devices = _context.Devices.Where(d => d.Name.Contains(textSearch)
                                                      && d.DepartmentId == departmentId).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList(); 

            var total = _context.Devices.Where(d => d.Name.Contains(textSearch)
                                                    && d.DepartmentId == departmentId).ToList();

            if (departmentId == department.Id)
            {
                devices = _context.Devices.Where(d => d.Name.Contains(textSearch)).OrderBy(d => d.Id)
                    .Skip(pageIndex * pageSize).Take(pageSize).ToList();

                total = _context.Devices.Where(d => d.Name.Contains(textSearch)).ToList();
            }


            if (devices.Count() == 0)
                return DataResponse<List<Device>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<Device>>.GetResultV2(1, "OK", total.Count, devices.Count, devices);
        }


        [HttpGet]
        [Route("api/devices/paging")]
        public DataResponse<List<Device>> GetDeviceByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var total = _context.Devices.ToList();

            var departmentQuery = _context.Devices.Where(d => d.Name.Contains(textSearch)).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            if (departmentQuery.Count == 0)
                return DataResponse<List<Device>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<Device>>.GetResultV2(1, "OK", total.Count, departmentQuery.Count, departmentQuery);
        }


        [HttpGet]
        [Route("api/devices/pagingv2")]
        public DataResponse<List<Device>> GetDeviceByPagingV2(int pageIndex, int pageSize, int departmentId, int statusId, string textSearch)
        {
            if (textSearch == null)
            {
                textSearch = "";
            }
            var total = _context.Devices.ToList();

            if (departmentId == 0 && statusId == 0)
            {
                var departmentQuery = _context.Devices.Where(d => d.Name.Contains(textSearch)).OrderBy(d => d.Id)
                    .Skip(pageIndex * pageSize).Take(pageSize).ToList();

                return DataResponse<List<Device>>.GetResultV2(1, "OK", total.Count, departmentQuery.Count, departmentQuery);
            }

            if (departmentId != 0 && statusId != 0)
            {
                var departmentQuery = _context.Devices.Where(d => d.Name.Contains(textSearch)
                                                                 && d.DepartmentId == departmentId
                                                                 && d.Status == statusId).OrderBy(d => d.Id)
                    .Skip(pageIndex * pageSize).Take(pageSize).ToList();

                return DataResponse<List<Device>>.GetResultV2(1, "OK", total.Count, departmentQuery.Count, departmentQuery);
            }

            return DataResponse<List<Device>>.GetResultV2(-1, "OK", total.Count, 0, null);

        }

        [HttpPut]
        [Route("api/devices/{id:int}")]
        public DataResponse<Device> UpdateDevice(int id, Device updateDevice)
        {
            if (!ModelState.IsValid)
                return DataResponse<Device>.GetResult(-1, "Model is not valid", null, null);

            var deviceInDb = _context.Devices.SingleOrDefault(d => d.Id == id);

            if (deviceInDb == null)
                return DataResponse<Device>.GetResult(-1, "Not Found", null, null);

            deviceInDb.Name = updateDevice.Name;
            deviceInDb.Model = updateDevice.Model;
            deviceInDb.Serial = updateDevice.Serial;
            deviceInDb.Country = updateDevice.Country;
            deviceInDb.Manufacture = updateDevice.Manufacture;
            deviceInDb.ProduceDate = updateDevice.ProduceDate;
            deviceInDb.ImportedDate = updateDevice.ImportedDate;
            deviceInDb.HandoverDate = updateDevice.HandoverDate;
            deviceInDb.ExpiredDate = updateDevice.ExpiredDate;
            deviceInDb.Status = updateDevice.Status;
            deviceInDb.Price = updateDevice.Price;
            deviceInDb.Note = updateDevice.Note;
            deviceInDb.ProviderId = updateDevice.ProviderId;
            deviceInDb.TypeId = updateDevice.TypeId;
            deviceInDb.DepartmentId = updateDevice.DepartmentId;

            _context.SaveChanges();

            return DataResponse<Device>.GetResult(1, "Updated", 1, updateDevice);
        }

        [HttpDelete]
        [Route("api/devices/{id:int}")]
        public DataResponse<Device> DeleteDevice(int id)
        {
            var deviceQuery = _context.Devices.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Device>.GetResult(-1, "Not Found", null);

            _context.Devices.Remove(deviceQuery);

            _context.SaveChanges();

            return DataResponse<Device>.GetResult(1, "Deleted", null);
        }

    }
}