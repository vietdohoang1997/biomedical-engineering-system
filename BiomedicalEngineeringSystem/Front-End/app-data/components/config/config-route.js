﻿app.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        // LOGIN
        .state('login',
            {
                url: '/login',
                templateUrl: 'Front-End/app-data/views/login/login.html'
            })
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home',
            {
                url: '/home',
                templateUrl: 'Front-End/app-data/views/home/partial-home.html',
                controller: 'HomeCtrl'
            })

        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('listDevice',
            {
                // we'll get to this in a bit     
                url: '/listDevice',
                templateUrl: 'Front-End/app-data/views/device/device-list.html',
                controller: 'DeviceCtrl'
            })

        .state('listDepartment',
            {
                // we'll get to this in a bit     
                url: '/listDepartment',
                templateUrl: 'Front-End/app-data/views/department/department-list.html',
                controller: 'DepartmentCtrl'
            })
        .state('listProvider',
            {
                // we'll get to this in a bit     
                url: '/listProvider',
                templateUrl: 'Front-End/app-data/views/provider/provider-list.html',
                controller: 'ProviderCtrl'
            })
        .state('listDeviceType',
            {
                // we'll get to this in a bit     
                url: '/listDeviceType',
                templateUrl: 'Front-End/app-data/views/device-type/device-type-list.html',
                controller: 'DeviceTypeCtrl'
            })
        .state('listAccessory',
            {
                // we'll get to this in a bit     
                url: '/listAccessory',
                templateUrl: 'Front-End/app-data/views/accessory/accessory-list.html',
                controller: 'AccessoryCtrl'
            })
        .state('listUser',
            {
                // we'll get to this in a bit     
                url: '/listUser',
                templateUrl: 'Front-End/app-data/views/user/user-list.html',
                controller: 'UserCtrl'
            })
        .state('listSchedule',
            {
                // we'll get to this in a bit     
                url: '/listSchedule',
                templateUrl: 'Front-End/app-data/views/maintain-schedule/device-schedule.html',
                controller: 'DeviceScheduleCtrl'
            })
        .state('listHistory',
            {
                // we'll get to this in a bit     
                url: '/listHistory',
                templateUrl: 'Front-End/app-data/views/maintain-history/maintain-history.html',
                controller: 'MaintainHistoryCtrl'
            })
        .state('listDeviceStatus',
            {
                // we'll get to this in a bit     
                url: '/listDeviceStatus',
                templateUrl: 'Front-End/app-data/views/maintain-history/device-status.html',
                controller: 'DeviceStatusCtrl'
            });



});