﻿app.controller('ProviderModalCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/providers/';
        $scope.Provider = item;


        var addProvider = function () {
            var apiRoute = baseUrl;
            var postData = $scope.Provider;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        var updateProvider = function () {
            var apiRoute = baseUrl + "/" + $scope.Provider.Id;
            var postData = $scope.Provider;
            var promise = crudService.put(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        $scope.save = function () {
            if (option[0].Type === 'add') {
                addProvider();
            }
            if (option[0].Type === 'edit') {
                updateProvider();
            }
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {

            if (option[0].Type === 'edit') {
                $scope.Title = 'CẬP NHẬT NHÀ CUNG CẤP';
            } else if (option[0].Type === 'add') {
                $scope.Title = 'THÊM MỚI NHÀ CUNG CẤP';
            }

        };

        initMain();

    }]);