﻿app.controller('DeviceModalCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/devices/';
        $scope.Device = item;

        var getDepartments = function () {
            var apiRoute = '/api/departments/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                    $scope.DEPARTMENTS = response.data.Data;
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        var getProviders = function () {
            var apiRoute = '/api/providers/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                    $scope.PROVIDERS = response.data.Data;
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        var getDeviceTypes = function () {
            var apiRoute = '/api/device_types/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                    $scope.DEVICETYPES = response.data.Data;
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        var addDevice = function () {
            var apiRoute = baseUrl;
            var postData = $scope.Device;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                    var dataResult = response.data;
                    $uibModalInstance.close(dataResult);
                },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        var updateDevice = function () {
            var apiRoute = baseUrl + "/" + $scope.Device.Id;
            var postData = $scope.Device;
            var promise = crudService.put(apiRoute, postData);
            promise.then(function (response) {
                    var dataResult = response.data;
                    $uibModalInstance.close(dataResult);
                },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        $scope.save = function () {
            if (option[0].Type === 'add') {
                addDevice();
            }
            if (option[0].Type === 'edit') {
                updateDevice();
            }
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {

            if (option[0].Type === 'edit') {
                $scope.Title = 'CẬP NHẬT THIẾT BỊ';
            } else if (option[0].Type === 'add') {
                $scope.Title = 'THÊM MỚI THIẾT BỊ';
            }
            getDepartments();
            getProviders();
            getDeviceTypes();
        };

        initMain();

        $scope.init = function () {
            $scope.datepickerOptions = {
                minDate: new Date('1900-01-01') 
            };
        };

    }]);