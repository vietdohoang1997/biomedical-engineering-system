﻿app.controller('DepartmentModalCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/departments/';
        $scope.Department = item;


        var addDepartment = function () {
            var apiRoute = baseUrl;
            var postData = $scope.Department;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                    var dataResult = response.data;
                    $uibModalInstance.close(dataResult);
                },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        var updateDepartment = function () {
            var apiRoute = baseUrl + "/" + $scope.Department.Id;
            var postData = $scope.Department;
            var promise = crudService.put(apiRoute, postData);
            promise.then(function (response) {
                    var dataResult = response.data;
                    $uibModalInstance.close(dataResult);
                },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        $scope.save = function () {
            if (option[0].Type === 'add') {
                addDepartment();
            }
            if (option[0].Type === 'edit') {
                updateDepartment();
            }
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {

            if (option[0].Type === 'edit') {
                $scope.Title = 'CẬP NHẬT KHOA PHÒNG';
            } else if (option[0].Type === 'add') {
                $scope.Title = 'THÊM MỚI KHOA PHÒNG';
            }

        };

        initMain();

    }]);