namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accessories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Size = c.Int(),
                        Number = c.Int(),
                        ImportedDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                        ProviderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Providers", t => t.ProviderId, cascadeDelete: false)
                .Index(t => t.ProviderId);
            
            CreateTable(
                "dbo.DeviceAccessories",
                c => new
                    {
                        DeviceId = c.Int(nullable: false),
                        AccessoryId = c.Int(nullable: false),
                        Note = c.String(),
                    })
                .PrimaryKey(t => new { t.DeviceId, t.AccessoryId })
                .ForeignKey("dbo.Accessories", t => t.AccessoryId, cascadeDelete: false)
                .ForeignKey("dbo.Devices", t => t.DeviceId, cascadeDelete: false)
                .Index(t => t.DeviceId)
                .Index(t => t.AccessoryId);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Model = c.String(),
                        Serial = c.String(),
                        Country = c.String(),
                        Manufacture = c.String(),
                        ProduceDate = c.DateTime(),
                        ImportedDate = c.DateTime(),
                        HandoverDate = c.DateTime(),
                        ExpiredDate = c.DateTime(),
                        Status = c.Int(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Note = c.String(),
                        ProviderId = c.Int(nullable: false),
                        TypeId = c.Int(),
                        DepartmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: false)
                .ForeignKey("dbo.DeviceTypes", t => t.TypeId)
                .ForeignKey("dbo.Providers", t => t.ProviderId, cascadeDelete: false)
                .Index(t => t.ProviderId)
                .Index(t => t.TypeId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DeviceTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DeviceGroup = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Providers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Representer = c.String(),
                        Mobile = c.Int(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MaintainSchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Time = c.DateTime(),
                        Status = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Devices", t => t.DeviceId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.DeviceId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        Mobile = c.Int(),
                        Address = c.String(),
                        Role = c.String(),
                        DepartmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: false)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        Content = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "UserId", "dbo.Users");
            DropForeignKey("dbo.MaintainSchedules", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.MaintainSchedules", "DeviceId", "dbo.Devices");
            DropForeignKey("dbo.Accessories", "ProviderId", "dbo.Providers");
            DropForeignKey("dbo.Devices", "ProviderId", "dbo.Providers");
            DropForeignKey("dbo.Devices", "TypeId", "dbo.DeviceTypes");
            DropForeignKey("dbo.DeviceAccessories", "DeviceId", "dbo.Devices");
            DropForeignKey("dbo.Devices", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.DeviceAccessories", "AccessoryId", "dbo.Accessories");
            DropIndex("dbo.Notifications", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "DepartmentId" });
            DropIndex("dbo.MaintainSchedules", new[] { "UserId" });
            DropIndex("dbo.MaintainSchedules", new[] { "DeviceId" });
            DropIndex("dbo.Devices", new[] { "DepartmentId" });
            DropIndex("dbo.Devices", new[] { "TypeId" });
            DropIndex("dbo.Devices", new[] { "ProviderId" });
            DropIndex("dbo.DeviceAccessories", new[] { "AccessoryId" });
            DropIndex("dbo.DeviceAccessories", new[] { "DeviceId" });
            DropIndex("dbo.Accessories", new[] { "ProviderId" });
            DropTable("dbo.Notifications");
            DropTable("dbo.Users");
            DropTable("dbo.MaintainSchedules");
            DropTable("dbo.Providers");
            DropTable("dbo.DeviceTypes");
            DropTable("dbo.Departments");
            DropTable("dbo.Devices");
            DropTable("dbo.DeviceAccessories");
            DropTable("dbo.Accessories");
        }
    }
}
