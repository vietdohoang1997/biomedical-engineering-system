namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixAddDeviceId : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Notifications", "DeviceId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Notifications", "DeviceId", c => c.Int(nullable: false));
        }
    }
}
