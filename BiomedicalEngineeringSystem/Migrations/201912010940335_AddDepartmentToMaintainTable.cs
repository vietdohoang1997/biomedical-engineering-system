namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentToMaintainTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MaintainSchedules", "Department", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MaintainSchedules", "Department");
        }
    }
}
