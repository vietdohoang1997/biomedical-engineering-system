namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeviceIdToNotificationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "DeviceId", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "DeviceId");
        }
    }
}
