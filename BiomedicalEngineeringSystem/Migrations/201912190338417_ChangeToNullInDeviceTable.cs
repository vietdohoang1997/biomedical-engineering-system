namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeToNullInDeviceTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Devices", "Price", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Devices", "ProviderId", c => c.Int());
            AlterColumn("dbo.Devices", "DepartmentId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Devices", "DepartmentId", c => c.Int(nullable: false));
            AlterColumn("dbo.Devices", "ProviderId", c => c.Int(nullable: false));
            AlterColumn("dbo.Devices", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
