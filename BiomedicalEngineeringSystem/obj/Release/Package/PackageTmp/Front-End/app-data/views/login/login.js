﻿app.controller('LoginCtrl', ['$scope', 'CrudService', '$uibModal', "toastr", "$location",
    function ($scope, crudService, $uibModal, $notifications, $location) {
        // Base Url 
        var baseUrl = '/api/users/';

        $scope.Login = function (username, password) {
            var apiRoute = baseUrl;
            var promise = crudService.login(apiRoute, username, password);
            promise.then(function (response) {
                    if (response.data.Code === 1) {
                        $scope.user = response.data;
                        localStorage.setItem("fullName", $scope.user.Data.FullName);
                        localStorage.setItem("userName", $scope.user.Data.UserName);
                        localStorage.setItem("departmentId", $scope.user.Data.DepartmentId);
                        $notifications.success("Đăng nhập thành công");
                        window.location = 'http://localhost:53823/index.html';
                    } else {
                        $notifications.error("Đăng nhập thất bại");
                    }
                },
                function (error) {
                    console.log("Error: " + error);
                });

        }

    }]);