﻿app.controller('UserModalCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/users/';
        $scope.User = item;

        var getDepartments = function () {
            var apiRoute = '/api/departments/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                    $scope.DEPARTMENTS = response.data.Data;
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        var addUser = function () {
            var apiRoute = baseUrl;
            var postData = $scope.User;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        var updateUser = function () {
            var apiRoute = baseUrl + "/" + $scope.User.Id;
            var postData = $scope.User;
            var promise = crudService.put(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        $scope.save = function () {
            if (option[0].Type === 'add') {
                addUser();
            }
            if (option[0].Type === 'edit') {
                updateUser();
            }
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {

            if (option[0].Type === 'edit') {
                $scope.Title = 'CẬP NHẬT NHÂN VIÊN';
            } else if (option[0].Type === 'add') {
                $scope.Title = 'THÊM MỚI NHÂN VIÊN';
            }

            getDepartments();

        };

        initMain();

    }]);