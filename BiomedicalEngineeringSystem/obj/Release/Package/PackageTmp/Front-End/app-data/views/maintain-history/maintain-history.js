﻿app.controller('MaintainHistoryCtrl', ['$scope', 'CrudService', '$uibModal', "toastr",
    function ($scope, crudService, $uibModal, $notifications) {
        // Base Url 
        var baseUrl = '/api/maintain_schedules/';
        $scope.maintainID = 0;

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  

        $scope.page = 0;

        var getMaintains = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                $scope.maintains = response.data;
                $scope.totalCount = response.data.TotalCount;
                $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
            },
                function (error) {
                    console.log("Error: " + error);
                });
        };


        var initData = function () {
            getMaintains();
        };

        $scope.Statuss = [
            { Status: 1, Name: "Chưa sửa" },
            { Status: 2, Name: "Đã sửa" },
            { Status: 3, Name: "Đã bàn giao" },
            { Status: 4, Name: "Chờ thanh lý" }
        ];

        $scope.changeDeviceStatus = function(item) {
            var apiRoute = baseUrl + item.Id;
            var postData = item;
            var promise = crudService.put(apiRoute, postData);
            promise.then(function (response) {
                    $notifications.success("Đã cập nhật", "Thông báo");
                    initData();
                },
                function (error) {
                    console.log("Error: " + error);
                });
        };


        $scope.pageChanged = function () {
            getMaintains();
        };

        $scope.changePageSize = function () {
            $scope.pageIndex = 1;
            getMaintains();
        };



        $scope.Search = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected + "&textSearch=" + $scope.textSearch;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                $scope.Maintains = response.data;
                $scope.totalCount = response.data.DataCount;
                $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        $scope.Delete = function (item) {
            var apiRoute = baseUrl + "/" + item.Id;
            var promise = crudService.delete(apiRoute);
            promise.then(function (response) {
                    var dataResult = response.data;
                    if (dataResult.Code === 1) {
                        initData();
                        $notifications.success("Đã xóa",
                            "Thông báo",
                            {
                                timeOut: 5000
                            });
                    } else {
                        $notifications.warning("Không xóa được",
                            "Thông báo",
                            {
                                timeOut: 5000
                            });
                    }
                },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;

        }

        initData();

    }]);