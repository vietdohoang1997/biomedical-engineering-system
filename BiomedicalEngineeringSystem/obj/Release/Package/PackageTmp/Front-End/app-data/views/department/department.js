﻿app.controller('DepartmentCtrl', ['$scope', 'CrudService', '$uibModal', "toastr",
    function ($scope, crudService, $uibModal, $notifications) {
        // Base Url 
        var baseUrl = '/api/departments/';
        $scope.departmentID = 0;

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  

        $scope.page = 0;

        var getDepartments = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                    $scope.departments= response.data;
                    $scope.totalCount = response.data.TotalCount;
                    $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
                },
                function (error) {
                    console.log("Error: " + error);
                });
        };

        $scope.pageChanged = function () {
            getDepartments();
        };

        $scope.changePageSize = function () {
            $scope.pageIndex = 1;
            getDepartments();
        };

        var initData = function () {
            getDepartments();
        }

        $scope.Add = function () {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/department/department-modal.html",
                controller: "DepartmentModalCtrl",
                size: "md",
                resolve: {
                    item: function () {
                        var obj = {};
                        return obj;
                    },
                    option: function () {
                        return [{ Type: "add" }];
                    }
                }
            });
            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Thêm mới thành công", "Thông báo");
                        initData();
                    }
                },
                function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.Update = function (item) {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/department/department-modal.html",
                controller: "DepartmentModalCtrl",
                size: "md",
                keyboard: false,
                backdrop: "static",
                resolve: {
                    item: function () {
                        return item;
                    },
                    option: function () {
                        return [{ Type: "edit" }];
                    }
                }
            });

            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Cập nhật thành công");
                    } else {
                        $notifications.warning("Có lỗi xảy ra");
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                },
                function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.Delete = function (item) {
            var apiRoute = baseUrl + "/" + item.Id;
            var promise = crudService.delete(apiRoute);
            promise.then(function (response) {
                var dataResult = response.data;
                if (dataResult.Code === 1) {
                    initData();
                    $notifications.success("Đã xóa",
                        "Thông báo",
                        {
                            timeOut: 5000
                        });
                } else {
                    $notifications.warning("Không xóa được",
                        "Thông báo",
                        {
                            timeOut: 5000
                        });
                }
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;

        }

        $scope.Search = function() {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected + "&textSearch=" + $scope.textSearch;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                    $scope.departments = response.data;
                    $scope.totalCount = response.data.DataCount;
                    $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        initData();

    }]);