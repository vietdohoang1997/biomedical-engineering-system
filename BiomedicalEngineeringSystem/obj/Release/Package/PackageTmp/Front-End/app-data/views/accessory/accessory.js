﻿app.controller('AccessoryCtrl', ['$scope', 'CrudService', '$uibModal', "toastr",
    function ($scope, crudService, $uibModal, $notifications) {
        // Base Url 
        var baseUrl = '/api/accessories/';
        $scope.accessoryID = 0;

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  

        $scope.page = 0;

        var getAccessories = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                $scope.accessories = response.data;
                $scope.totalCount = response.data.TotalCount;
                $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
            },
                function (error) {
                    console.log("Error: " + error);
                });
        };

        $scope.getStatus = function(id) {
            if (id === 1) {
                return "Đang sử dụng";
            }

            if (id === 2) {
                return "Hỏng";
            }

            if (id === 3) {
                return "Đã tiếp nhân";
            }
        };

        $scope.pageChanged = function () {
            getAccessories();
        };

        $scope.changePageSize = function () {
            $scope.pageIndex = 1;
            getAccessories();
        };

        var initData = function () {
            getAccessories();
        }

        $scope.Add = function () {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/accessory/accessory-modal.html",
                controller: "AccessoryModalCtrl",
                size: "md",
                resolve: {
                    item: function () {
                        var obj = {};
                        return obj;
                    },
                    option: function () {
                        return [{ Type: "add" }];
                    }
                }
            });
            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Thêm mới thành công", "Thông báo");
                        initData();
                    }
                },
                function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.Update = function (item) {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/accessory/accessory-modal.html",
                controller: "AccessoryModalCtrl",
                size: "md",
                keyboard: false,
                backdrop: "static",
                resolve: {
                    item: function () {
                        return item;
                    },
                    option: function () {
                        return [{ Type: "edit" }];
                    }
                }
            });

            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Cập nhật thành công");
                    } else {
                        $notifications.warning("Có lỗi xảy ra");
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                },
                function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.Delete = function (item) {
            var apiRoute = baseUrl + "/" + item.Id;
            var promise = crudService.delete(apiRoute);
            promise.then(function (response) {
                var dataResult = response.data;
                if (dataResult.Code === 1) {
                    initData();
                    $notifications.success("Đã xóa",
                        "Thông báo",
                        {
                            timeOut: 5000
                        });
                } else {
                    $notifications.warning("Không xóa được",
                        "Thông báo",
                        {
                            timeOut: 5000
                        });
                }
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;

        }

        $scope.Search = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected + "&textSearch=" + $scope.textSearch;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                $scope.accessories = response.data;
                $scope.totalCount = response.data.DataCount;
                $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        initData();

    }]);