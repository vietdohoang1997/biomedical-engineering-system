﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class DeviceType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string DeviceGroup { get; set; }
    }
}