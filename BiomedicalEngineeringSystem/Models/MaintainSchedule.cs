﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class MaintainSchedule
    {
        [Key]
        public int Id { get; set; }

        public int? DeviceId { get; set; }
        
        public int? UserId { get; set; }

        public DateTime? Time { get; set; }

        public int? Status { get; set; }

        public string UserName { get; set; }

        public string DeviceName { get; set; }

        public string Department { get; set; }
    }
}